# Contributors and Supporters

I just wanted to create a space to say thank you to those that have directly contributed to the materials here or have provided support in some way. 

- [Stephanie Burdine](https://www.linkedin.com/in/stephanieburdine/)
- [Sydney Gregory](https://www.linkedin.com/in/sydney-gregory/)
- [Taylor Bikowski](https://www.linkedin.com/in/taylorbikowski/)
- [Lisa Wolfe](https://www.linkedin.com/in/lisa-wolfe-5b4b375/)
- [Anne Monaghan-Baker](https://www.linkedin.com/in/anne-monaghan-baker/)
- Sam Knuth