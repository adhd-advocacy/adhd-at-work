# F.A.Q. - Frequently Asked Questions

## What is ADHD?

## How can I help an employee with ADHD?

## What are the "strengths" of individuals with ADHD?
While these may be true of the community, please understand each individual is different. They come to work with their own experiences. Some are just starting their journey with ADHD, others have been working with their ADHD for a long time. Some people were supported through their journey, others were not and may be told they were lazy, stupid, irresponsible, childish, etc.

- Ability to hyperfocus on things that they find interesting
- Willingness to take risks
- Spontaneous and flexible
- Good in a crisis
- Excel at divergent thinking 
- Creative ideas - outside-the-box thinkers
- Relentless energy when fully engaged
- Often optimistic
- Motivated by short-term deadlines
- Eye for detail 

Everyone with ADHD is different. Although these qualities tend to manifest in people with ADHD, they should not be the bench mark that someone is judged by.
