# Glossary

This is a space to define and explore language often used in the ADHD community. To have this important conversation we need to have a shared language and not assume we understand what is meant but instead explicitly define terms where we can. 

## ADD
Attention Deficit Disorder; an older term which is still used by some for those that don't exhibit hyperactivity.

## ADHD
Attention Deficit/Hyperactivity Disorder

### ADHD-Combined Type
A subtype of ADHD where an individual manifests both inattentive and hyperactive/impulsive behaviors.

### ADHD-Hyperactive/Impulsive
A subtype of ADHD where an individual exhibits hyperactive/impulsive behavior but lacks symptoms of inattentive behavior.

### ADHD-Inattentive
A subtype of ADHD where an individual exhibits inattentve behavior but lacks symptoms of hyperactive/impulsive behavior.

## Attentional Bias
Preference for paying attention to certain objects, thoughts, and activities that one finds interesting.

## Comorbidity
Two of more disorders occurring in an individual at the same time.

## Distractability
The inability to sustain attention on the task at hand. 

## Executive Function
A subset of mental processes needed when going on autopilot or relying on intuition would not serve the best interests of the individual. There are three core components:

- Inhibitory control
- Working memory
- Cognitive flexibility

## Hyperfocus
A deep and intense mental concentration fixated on an activity, specific event, or topic.
