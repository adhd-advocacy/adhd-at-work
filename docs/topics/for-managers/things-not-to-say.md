# Things you should avoid saying to someone with ADHD

## Key points
**Don't**

- Don't dismiss the condition.
    - Avoid minimizing the effect ADHD has on someone's life.
- Don't suggest that ADHD is overdiagnosed. 
- Don't criticize ADHD symptoms.
- Don't blame lack of discipline.
- Don't make comparisons.
- Don't feel like you need to know a lot about ADHD in order to work with someone with ADHD. 

**Do**

- Do show curiousity instead of judgment.
- Do show compassion.
- Do show an interest in partnering with the employee to make the situation mutually beneficial.
- Do be clear about expectations.
- Do be careful about language. Language matters.
    - Example: Instead of saying "what questions do you have?" you can say "do you have any questions?"

## Things to avoid saying

1. ADHD isn't a real disorder.
1. Everyone gets distracted sometimes.
1. Why can't you be normal?
1. Why are you like this?
1. Sorry, I'm having a ADD moment.
1. Aren't we all a little ADHD?
1. OK but doesn't everyone think they have ADHD?
1. You just need to work harder.
1. You just need to be more organized.
1. You're not even trying.
1. Why can't you just focus? Aren't you supposed to be able to hyperfocus?
1. You just need to be a better listener.
1. People with ADHD have an unfair advantage.
1. I would be better at work if I took ADHD meds too. 
1. People with ADHD are just lazy.
1. If you really cared you would have heard me the first time.
1. You don't need more time, you just need to work faster.
1. What's taking you so long?
1. ADHD is just an excuse for not doing well at [insert task here].
1. Having ADHD is not an excuse.
1. Those are just excuses.
1. "Stop overreacting" or "You're overreacting".
1. Stop being so emotional.
1. Don't be crazy.
1. Can you stop fidgeting?
1. How could you forget something so important?
1. Did you forget to take your meds today?
1. It's like you have no filter.
1. You are being spacey.
1. You are being a spaz. 
1. If you just did [insert opinion here] you wouldn't need medication.
1. You don't look like someone with ADHD.
1. But you're so smart, how do you have ADHD?
1. Aren't people with ADHD supposed to be more creative?
1. You don't have ADHD you're just [insert adjective].
1. You just need to learn to take care of yourself.
1. Isn't ADHD a thing kids have?
