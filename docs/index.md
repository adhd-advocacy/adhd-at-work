# ADHD at work

This is a resource for employees with ADHD to help them navigate the workplace and for managers to help them nuture and lead employees that have ADHD. Many of us in the ADHD community spend time trying to figure out how to navigate a world and workplace culture that doesn't seem built for us. The community realizes that sometimes when we are vocal about our needs that managers may not understand how to meet those needs. The community also realizes that some members may not have the bandwidth to explain everything to their manager. It is our hope that this will strike a nice balance between the needs of leaders that want to understand and the individuals that don't have the capacity to teach. 

Before we begin, the scope of this information and limitation need to be acknowledged. This was not written by a "professional" unless you consider living with ADHD evidence enough of a certain level of expertise. The experiences are limited to those working in tech in North America. Culture influences a number of things and it feels like the responsible thing to do by acknowledging the limited perspective of this information. It keeps the conversation honest. As more experiences are pulled into this resource we will update the limitations and scope of this information. 

</br>
If you have questions or want to get involved.

[Email us](mailto:doublejeem@gmail.com){: .md-button .md-button--primary }